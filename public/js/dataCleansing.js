// Initial the radio value of interest rate policy selection:
// 1: One stage (default)
// 2: Two stages
var rateStage = 1;
var blurTimeout;

// 1. Examine and clean-up the formation of parameters adopting class 'integerVal'/'decimalVal'
// 2. Action to move to the top of page
$(document).ready(function () {
  // For class 'integerVal', integer only and remove leading 0
  //
  // Also add feature to show thousands separators in real time
  $('.integerVal').on('input', function () {
    var value = $(this).val().replace(/\D/g, '');
    var formatter = new Intl.NumberFormat('en-US');

    let cursorPosition = this.selectionStart;
    let originalLength = this.value.length;
   
    // Remove leading 0s for special scenario:
    // 1,000,123 then remove leading 1
    // -- Louis 2024/0320
    while (value.startsWith('0')) {
      //console.log(value);
      value = value.substr(1);
    }

    $(this).val(value);

    if (value != "") {
      this.value = this.value
          .replace(/[^\d]/g, '')
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      let newLength = this.value.length;
      cursorPosition += (newLength - originalLength);

      this.setSelectionRange(cursorPosition, cursorPosition);
    }
  });

  // For class 'decimalVal', handle decimal values;
  $('.decimalVal').on('input', function () {
    var value = $(this).val();

    value = value.replace(/[^0-9.]/g, ''); // Keep numbers and decimal dots only
    value = value.replace(/^0+/, '0'); // Remove multiple leading 0
    value = value.replace(/(\..*)\./g, '$1'); // One decimal dot only
    value = value.replace(/^\.+/, ''); // Remove decimal dot located at first place

    // Remove leading 0 unless followed by a decimal dot
    if (!value.startsWith('0.') || value === '0.') {
      value = value.replace(/^0+([1-9])/, '$1');
    }

    $(this).val(value);
  });

  // Scroll to page top
  $('#scrollToTop').click(function () {
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
  });
});

// Initialize/restore the parameters when the page is displayed
window.onpageshow = function () {
    resetAll();
};

// Examine input box 'moneyLoaned'
// 1. Check when blur(), 10 <= value <= 300, or < 'serviceCharge'
// 2. Change box border to be red, and show error error message 
// 3. Wrap up as function which would be called by calculation button
//
// Update range to 5 <= value <= 300 by request -- Louis 2024/0307
function moneyLoanedCheck () {
  // Replace regx (",", "") to (/\D/g, '') to handle multiple thousands separators scenario
  // -- Louis 2024/0307
  var val = parseInt($('#moneyLoaned').val().replace(/\D/g, ''));
  var errorMessage = '貸款金額最低 5 萬, 最高 500 萬';

  var formatter = new Intl.NumberFormat('en-US');
  
  if (val >= 50000 && val <= 5000000) {
    $('#moneyLoaned').css('border-color', 'black');
    $('#moneyLoaned_error').hide();
    $("input[name='moneyLoaned']").val(formatter.format(val));

    return true;
  } else {
    $('#moneyLoaned').css('border-color', 'red');
    $('#moneyLoaned_error').text(errorMessage);
    $('#moneyLoaned_error').show();

    return false;
  }
}
// Examine input box 'totalPeriod'
// 1. Check when blure(), 24 <= value <= 84
// 2. Change box border to be red, and show error error message 
// 3. Wrap up as function which would be called by calculation button
//
// Update range to 12 <= value <= 84 by request -- Louis 2024/0307
function totalPeriodCheck () {
  var errorMessage = '貸款期間最低 12 期, 最高 84 期';
  var val = parseInt($('#totalPeriod').val());

  if (val >= 12 && val <= 84) {
    $('#totalPeriod').css('border-color', 'black');
    $('#totalPeriod_error').hide();
    
    // Reset value of end-month of the 2nd stage and error block
    $('#twoRatesStage2End').val(val);
    $('#twoRates_error').hide();

    return true;
  } else {
    $('#totalPeriod').css('border-color', 'red');
    $('#totalPeriod_error').text(errorMessage);
    $('#totalPeriod_error').show();
    
    // Also handle 2nd stage error block 
    let _errorMessage = '請輸入各階段起始月份與利率 (0% ~ 16%)';
    $('#twoRatesStage2End').val("");
    $('#twoRates_error').text(_errorMessage);
    $('#twoRates_error').show();

    return false;
  }
}

// Examine input box 'serviceCharge'
// 1. Check when blure(), > 10000 (might be changed)
// 3. Change box border to be red, and show error error message 
// 4. Wrap up as function which would be called by calculation button
function serviceChargeCheck () {
  var errorMessage = '貸款費用不可大於 1 萬元';
 // Replace regx (",", "") to (/\D/g, '') to handle multiple thousands separators scenario
 // -- Louis 2024/0307
  var val = parseInt($('#serviceCharge').val().replace(/\D/g, ''));
  var formatter = new Intl.NumberFormat('en-US');

  if (isNaN(val) || val < 10000) {
    $('#serviceCharge').css('border', 'solid 1px black');
    $('#serviceCharge_error').hide();
    if (!isNaN(val)) { $('#serviceCharge').val(formatter.format(val)); }
    
    return true;
  } else {
    $('#serviceCharge').css('border', 'solid 1px red');
    $('#serviceCharge_error').text(errorMessage);
    $('#serviceCharge_error').show();
    $("input[name='serviceCharge']").val(formatter.format(val));
    
    return false;
  }
}

// Examine the parameter of one-stage interest rate policy
// 1. Check interest rate value, 0 <= value <= 16
function oneRateCheck () {
  var rate = parseFloat($('#oneRateVal').val());
  var errorMessage = '貸款利率最低 0%, 最高 16%'

  if (rate >= 0 && rate <= 16) {
    $('#oneRateVal').css('border', 'solid 1px black');
    $('#oneRate_error').text(errorMessage);
    $('#oneRate_error').hide();

    return true;
  } else {
    $('#oneRateVal').css('border', 'solid 1px red');
    $('#oneRate_error').text(errorMessage);
    $('#oneRate_error').show();
    
    return false;
  }
}

// Examine the parameters of two-stage interest rate policy
// 1. Check interest rate value , 0 <= value <= 16
// 2. Check end-month of the 1st stage is valid
function twoRatesCheck () {
  var checkFlag = true;
  var s1Rate = parseFloat($('#twoRatesStage1Val').val());
  var s2Rate = parseFloat($('#twoRatesStage2Val').val());
  var errorMessage = '請輸入各階段起始月份與利率 (0% ~ 16%)';

  if ($('#twoRatesStage1End').val() == "") { 
    $('#twoRatesStage1End').css('border-color', 'red');
    checkFlag = false; 
  } else {
    $('#twoRatesStage1End').css('border-color', 'black');
  }
  if ($('#twoRatesStage2End').val() == "") { checkFlag = false; }

  if (s1Rate >= 0 && s1Rate <= 16) {
    $('#twoRatesStage1Val').css('border-color', 'black');
  } else {
    $('#twoRatesStage1Val').css('border-color', 'red');
    checkFlag = false;
  }

  if (s2Rate >= 0 && s2Rate <= 16) {
    $('#twoRatesStage2Val').css('border-color', 'black');
  } else {
    $('#twoRatesStage2Val').css('border-color', 'red');
    checkFlag = false;
  }

  if (!checkFlag) {
    $('#twoRates_error').text(errorMessage);
    $('#twoRates_error').show();
  } else {
    $('#twoRates_error').hide();
  }
  
  return checkFlag;
}
// Add timeout mechanism to fix issue (ugly):
// validation function called by calBtn would not be triggered if input box is defocused 
// by clicking calBtn
$("#moneyLoaned").blur(function () { 
  blurTimeout = setTimeout(function () {
    moneyLoanedCheck();
  }, 100);
});

$("#totalPeriod").blur(function () { 
  blurTimeout = setTimeout(function () {
    totalPeriodCheck(); 
  }, 100);
});

// Remove service charge by request -- Louis 2024/0307
/*
$("#serviceCharge").blur(function () { 
  blurTimeout = setTimeout(function () {
    serviceChargeCheck();
  }, 100);
});
*/

$("#oneRateVal").blur(function () {
  blurTimeout = setTimeout(function () {
    oneRateCheck('blur');
  }, 100);
});

// Resume rateSelection status when radio button clicked
// 1. Clean up input field 
// 2. Hide error block 
// 3. Reset border color to black
$('.rateSelection').change(function () {
  rateStage = parseInt($("input[type=radio]:checked").val());

  $('.oneRate_block, .twoRates_block').hide();
  
  $('#twoRatesStage1End').css('border-color', 'black');
  
  $('#oneRateVal').css('border-color', 'black');
  $('#twoRatesStage1Val').css('border-color', 'black');
  $('#twoRatesStage2Val').css('border-color', 'black');

  $('#oneRate_error').hide();
  $('#twoRates_error').hide();

  $('input[name="rate"]').val("");
  $('input[name="month"]').val("");
  
  if ($('#oneRate').prop('checked')) { $('.oneRate_block').show(); }
  else { $('.twoRates_block').show(); }
});

// Update month values accordingly when related parameters changed
$(document).on("input", "input[name=month]", function () {
  var month = parseInt($(this).val());
  var period = parseInt($("#twoRatesStage2End").val());

  if (month < period) {
    $("#twoRatesStage2Start").val(month + 1);
  } else {
    $("#twoRatesStage1End").val("");
    $("#twoRatesStage2Start").val("");
  }
});

// Button to trigger loan calculation
$(".calBtn").click(function () {
  var checkFlag = true;

  // Check everything before starting calculation 
  if (!moneyLoanedCheck()) { 
    clearTimeout(blurTimeout);
    checkFlag = false; 
  }
  if (!totalPeriodCheck()) { 
    clearTimeout(blurTimeout);
    checkFlag = false; 
  }

  // Remove service charge by request -- Louis 2024/0307
  /*
  if (!serviceChargeCheck()) { 
    clearTimeout(blurTimeout);
    checkFlag = false; 
  }
  */

  if ($('#oneRate').prop('checked')) {
    clearTimeout(blurTimeout);
    if(!oneRateCheck()) {
      checkFlag = false; 
    }
  } else {
    if (!twoRatesCheck()) { 
      checkFlag = false; 
    }
  }
  
  if (checkFlag) {
    loanCalculation();

    $('.result_block').show();
  } else {
    $('.result_block').hide();
  }
  $('html, body').animate({ scrollTop: $('.result_block').offset().top }, 'slow');
});

// Button to reset input and move back to form
$('.btnReset').click(function (e) {
  e.preventDefault();
  resetAll();
  $('html, body').animate({ scrollTop: $('.form_block').offset().top }, 'slow');
});

// Initialize/restore the form_block/result_block to original state
function resetAll() {
  $('.result_block').hide();
  $('input[type=text]').val('');
  $('#twoRatesStage1Start').val("1");
  $('input[type=text]').css('border-color', 'black');
  $('input[readonly]').css('border-color', '#acacac');
  $('.input_block_error').hide();
  $("#oneRate").click();
}

// Calculae loan detail
function loanCalculation() {
  // Replace regx (",", "") to (/\D/g, '') to handle multiple thousands separators scenario
  // Remove service charge by request
  // -- Louis 2024/0307
  //var serviceChargeVal = parseInt($('#serviceCharge').val().replace(/\D/g, ''));
  var gracePeriodVal = parseInt($('#gracePeriod').val());

  // Data structure storing parameters for loan calculation
  var dataSet = {
    totalPeriod: parseInt($('#totalPeriod').val()),
    // Update the unit from 10 thousands digit to units digit by request
    // -- Louis 2027/0307
    //moneyLoaned: parseInt($('#moneyLoaned').val().replace(/,/g, '')) * 10000,
    moneyLoaned: parseInt($('#moneyLoaned').val().replace(/\D/g, '')),
    rateStage: parseInt(rateStage),
    // Remove service charge by request -- Louis 2024/0307
    //serviceCharge: isNaN(serviceChargeVal) ? 0 : serviceChargeVal,
    serviceCharge: 0,
    rateArray: [], // Array to save interest rate(s)
    stageStartArray: [], // Array to save the start period of each stage
    stageEndArray: [], // Array to save the end period of each stage

    //gracePeriod: isNaN(gracePeriodVal) ? 0 : gracePeriodVal,
  }

  switch (rateStage) {
    case 1:
      dataSet.rateStage = 1;
      dataSet.stageStartArray = [1];
      dataSet.stageEndArray = [dataSet.totalPeriod];
      dataSet.rateArray = [parseFloat($('#oneRateVal').val())];

      calculateResult(dataSet);
      break;
    case 2:
      dataSet.rateStage = 2;
      dataSet.stageStartArray = [parseInt($('#twoRatesStage1Start').val()), parseInt($('#twoRatesStage2Start').val())];
      dataSet.stageEndArray = [parseInt($('#twoRatesStage1End').val()), parseInt($('#twoRatesStage2End').val())];
      dataSet.rateArray = [parseFloat($('#twoRatesStage1Val').val()), parseFloat($('#twoRatesStage2Val').val())];
      
      calculateResult(dataSet);
      break;
    default:
      // Initialize
      dataSet.stageStartArray= [];
      dataSet.stageEndArray = [];
      dataSet.rateArry = [];
  }
}
