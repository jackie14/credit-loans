function calculateMonthlyInterest(loanAmount, annualInterestRate) {
  return Math.round(loanAmount * (annualInterestRate / 100 / 12));
}

function calculateMonthlyPayment(loanAmount, annualInterestRate, months) {
  const monthlyInterestRate = annualInterestRate / 100 / 12;
  return Math.round(loanAmount * monthlyInterestRate / (1 - Math.pow(1 + monthlyInterestRate, -months)));
}

function calculateResult(dataSet) {
  //console.log(dataSet);

  var totalInterest = 0;
  var totalPrincipal = 0;
  var totalPayments = 0;
  var cashFlows = []; // Initialize cash flow for IRR calculation
  var tableContent = ""; // HTML content for detail table
  var mPaymentContent = ""; // HTML content for monthly payment section
  var yearlyRateContent = ""; // HTML content for annual interest rate

  const loanAmount = parseFloat(dataSet.moneyLoaned);
  const totalMonths = dataSet.totalPeriod;
  const loanFee = parseFloat(dataSet.serviceCharge);

  var formatter = new Intl.NumberFormat('en-US');
  
  cashFlows.push(-loanAmount+loanFee);

  // One stage interest rate policy
  if (dataSet.rateStage == 1) {
    const interest1 = parseFloat(dataSet.rateArray[0]);

    var monthlyPayment = calculateMonthlyPayment(loanAmount, interest1, totalMonths);

    // Form monthly payment block content
    mPaymentContent = mPaymentContent + 
      '<div class="mPayment"><div style="margin-right: 5px;">第 1~' + totalMonths + ' 期</div>' +
      '<div>' + formatter.format(monthlyPayment) + ' 元 </div></div>'

    let remainingLoan = loanAmount;

    for (let i = 1; i <= totalMonths; i++) {
      let monthlyInterest = calculateMonthlyInterest(remainingLoan, interest1);
      let principalPayment = monthlyPayment - monthlyInterest;

      //當 i 為最後一期時，剩餘本金為 0
      if (i === totalMonths) {
        principalPayment = remainingLoan;
        monthlyPayment = principalPayment + monthlyInterest;
        remainingLoan = 0;
      } else {
        remainingLoan -= principalPayment;
      }

      totalInterest += monthlyInterest;
      totalPrincipal += principalPayment;
      totalPayments += monthlyPayment;
      cashFlows.push(monthlyPayment);
      
      tableContent += '<tr><td class="text-left">' + i +
        '</td><td>' + formatter.format(principalPayment) + '</td>' +
        '<td>' + formatter.format(monthlyInterest) + '</td>' +
        '<td>' + formatter.format(monthlyPayment) + '</td>' +
        '<td>' + formatter.format(remainingLoan) + '</td></tr>'
    }
  } else if (dataSet.rateStage == 2) {
    //console.log(dataSet.rateStage);

    const interest1 = parseFloat(dataSet.rateArray[0]);
    const interest2 = parseFloat(dataSet.rateArray[1]);
    const months1 = parseInt(dataSet.stageEndArray[0]);
    const months2 = totalMonths - months1;
    let remainingLoan = loanAmount;

    // 計算第一階段的月付金額
    let monthlyPayment1 = calculateMonthlyPayment(loanAmount, interest1, totalMonths);

    // Form 1st stage monthly payment block content
    mPaymentContent = mPaymentContent + 
      '<div class="mPayment"><div style="margin-right: 5px;">第 1~' + months1 + ' 期</div>' +
      '<div>' + formatter.format(monthlyPayment1) + ' 元 </div></div>'

    for (let i = 1; i <= totalMonths; i++) {
      let monthlyInterest, principalPayment, monthlyPayment;
      
      if (i <= months1) {
        monthlyInterest = calculateMonthlyInterest(remainingLoan, interest1);
        monthlyPayment = monthlyPayment1;
      } else {
        if (i === months1 + 1) {
          // 第二階段開始時重新計算月付金額
          monthlyPayment1 = calculateMonthlyPayment(remainingLoan, interest2, months2);

          // Form 2nd stage monthly payment block content
          mPaymentContent = mPaymentContent + 
            '<div class="mPayment"><div style="margin-right: 5px;">第 ' + (months1 + 1) + '~' + totalMonths + ' 期</div>' +
            '<div>' + formatter.format(monthlyPayment1) + ' 元 </div></div>'
        }
        monthlyInterest = calculateMonthlyInterest(remainingLoan, interest2);
        monthlyPayment = monthlyPayment1;
      }

      //當 i 為最後一期時，剩餘本金為 0
      if (i === totalMonths) {
        principalPayment = remainingLoan;
        monthlyPayment = principalPayment + monthlyInterest;
        remainingLoan = 0;
      }
      else {
        principalPayment = monthlyPayment - monthlyInterest;
        remainingLoan -= principalPayment;
      }
  
      totalInterest += monthlyInterest;
      totalPrincipal += principalPayment;
      totalPayments += monthlyPayment;
      cashFlows.push(monthlyPayment);

      tableContent += '<tr><td class="text-left">' + i +
        '</td><td>' + formatter.format(principalPayment) + '</td>' +
        '<td>' + formatter.format(monthlyInterest) + '</td>' +
        '<td>' + formatter.format(monthlyPayment) + '</td>' +
        '<td>' + formatter.format(remainingLoan) + '</td></tr>'
    }
  }
  
  // Monthly payment block
  $('#monthlyPayment').html(mPaymentContent);
  
  // Calculate annual interest rate
  var temp2 = calculateAnnualizedRate(cashFlows, 0.01) * 12 * 100;
  var lstValues = Math.round(temp2 * Math.pow(10, (2 || 0) + 1) / 10) / Math.pow(10, (2 || 0));
  // Annual interest rate block
  if ($('#annualInterestRate') !== 0) {
    $('#annualInterestRate').html('<div class="">' + lstValues + '%</div>');
  }

  // The last summary row
  // Remove summary by request -- Louis 2024/0307
  /*
  tableContent += '<tr><td>' + '總計' + 
    '</td><td>' + formatter.format(Math.round(totalPrincipal)) + '</td>' +
    '<td>' + formatter.format(Math.round(totalInterest)) + '</td>' +
    '<td>' + formatter.format(totalPayments) + '</td>' +
    '<td>' + '-' + '</td></tr>';
  */
  
  // Table(tbody) block
  $('tbody').empty();
  $('tbody').append('<tr>' +
    '<th style="width: 10%;">' + '期別' + '</th>' +
    '<th style="width: 22.5%;">' + '應還本金' + '</th>' +
    '<th style="width: 22.5%;">' + '應付利息' + '</th>' +
    '<th style="width: 22.5%;">' + '應付本息' + '</th>' +
    '<th style="width: 22.5%;">' + '剩餘本金' + '</th>' +
    '</tr>' + tableContent);
}

// IRR calculation
function calculateAnnualizedRate(cashFlows, initialGuess) {
  const calculateNetPresentValue = function (cashFlows, timePeriods, interestRate) {
    let netPresentValue = cashFlows[0];
    const adjustedRate = interestRate + 1;
    for (let j = 1; j < cashFlows.length; j++) {
        netPresentValue += cashFlows[j] / Math.pow(adjustedRate, (timePeriods[j] - timePeriods[0]) / 365);
    }
    return netPresentValue;
  }
  const derivativeNetPresentValue = function (cashFlows, timePeriods, interestRate) {
    const adjustedRate = interestRate + 1;
    let derivativeValue = 0;
    for (let j = 1; j < cashFlows.length; j++) {
        const timeFraction = (timePeriods[j] - timePeriods[0]) / 365;
        derivativeValue -= timeFraction * cashFlows[j] / Math.pow(adjustedRate, timeFraction + 1);
    }
    return derivativeValue;
  }
  let timePeriods = [];
  let hasPositive = false;
  let hasNegative = false;
  for (let i = 0; i < cashFlows.length; i++) {
    timePeriods[i] = (i === 0) ? 0 : timePeriods[i - 1] + 365;
    if (cashFlows[i] > 0) hasPositive = true;
    if (cashFlows[i] < 0) hasNegative = true;
  }
  if (!hasPositive || !hasNegative) return 'Invalid Input';
  let guessRate = (typeof initialGuess === 'undefined') ? 0.1 : initialGuess;
  let interestRate = guessRate;
  const tolerance = 1e-10;
  const maxIterations = 50;
  let newInterestRate, difference, netPresentValue;
  let iterationCount = 0;
  let continueIteration = true;
  do {
    netPresentValue = calculateNetPresentValue(cashFlows, timePeriods, interestRate);
    newInterestRate = interestRate - netPresentValue / derivativeNetPresentValue(cashFlows, timePeriods, interestRate);
    difference = Math.abs(newInterestRate - interestRate);
    interestRate = newInterestRate;
    continueIteration = (difference > tolerance) && (Math.abs(netPresentValue) > tolerance);
  } while (continueIteration && (++iterationCount < maxIterations));
  if (continueIteration) return 'Calculation Error';
  return interestRate;
}
